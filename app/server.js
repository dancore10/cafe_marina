/*
	* Define and require all vars to use
*/
var figlet = require('figlet');
var shell = require('shelljs');
var colors = require('colors');
var morgan = require('morgan');
var express = require('express');
var http = require('http');
var path = require('path');

var app = express();
var server = http.createServer(app);

var bodyParser = require('body-parser');
var cookieParser = require('cookie-parser');


/*
	* Set port and host for project
*/
const PORT = 3000;
const HOST = 'localhost';


/*
	* Confuguration for use engine, cookie, morgan and indent json 
*/
let ejs = require('ejs');
app.set('view engine', 'ejs');
app.use(bodyParser.json());
app.set(bodyParser.urlencoded({extended: false}));
app.use(cookieParser());
app.set('json spaces', 4);
app.use(morgan('dev'));

/*
	* set public folder
*/
app.use(express.static(path.join(__dirname, 'frontend/public')));

/*
	* Define routes folders
*/

var viewsRoutes = require("./backend/routes/viewsRoutes");
app.use('/', viewsRoutes);
// app.use('/api', apiRoutes);

/*
	* Functions for saluet in console with figlet
*/
function saluet(){
	figlet('MarinaCoffee', (err, data)=>{
		if (err) {
			console.log("Something went wrong...");
			console.dir(err);
			return;
		}
		console.log((data).green);
		console.log(('"Coffee Website for MarinaCoffee"').red.underline);
	});
}


/*
	* Getting port, host and function saluet after the clear console
*/
server.listen(PORT, ()=>{
	shell.exec('clear');
	saluet();
	console.log("Lisent on", HOST+':'+PORT);
});