'use stric'
var express = require('express'),
	router = express.Router(),
	path = require('path'),
	frontFolder = path.join(__dirname+'/../../frontend'),
	publicFolder = path.join(frontFolder+'/public'),
	viewsFolder = path.join(frontFolder+'/views');
    /*················
        CONTROLLERS
    ·················*/

router.get('/', function(req, res) {
	res.render(viewsFolder+'/home/index');
});


/*
	* petition Favicon
*/
router.get('/favicon.ico', function(req, res) {
	res.status(200).sendFile(publicFolder+'/img/favicon.ico');
});

/*
	* Route 404 Not Found
*/
router.all('*', (req, res)=>{
		res.status(404).sendFile(publicFolder+'/img/404.png');
		// res.status(500).sendFile(publicFolder+'/img/500.jpg');
});

module.exports = router;