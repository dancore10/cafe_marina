add:
	cd app && yarn add ${p}
	
install:
	cd app && yarn install
	clear

run:
	cd app && nodemon
	clear

build:
	docker-compose -f docker/docker-compose.yml build
	clear

up:
	docker-compose -f docker/docker-compose.yml up
	clear

bump:
	docker-compose -f docker/docker-compose.yml up --build
	clear

down:
	docker-compose -f docker/docker-compose.yml down
	docker-compose -f docker/docker-compose.yml stop
	clear